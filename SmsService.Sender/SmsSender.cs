﻿using System.Threading.Tasks;
using SmsService.ServiceModel;
using SmsService.ServiceModel.Interfaces;
using SmsService.ServiceModel.Types;

namespace SmsService.DummySender
{
    public class SmsSender : ISmsSender
    {
        private readonly ISmsRepository _repo;

        public SmsSender(ISmsRepository repo)
        {
            _repo = repo;
        }

        public async Task<SendResponse> Send(SmsDTO sms)
        {
            var status =  await _repo.SaveSms(sms);
            return new SendResponse { SendStatus = status };
        }
    }
}
