using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Funq;
using ServiceStack;
using ServiceStack.Text;
using SmsService.DummySender;
using SmsService.Repository;
using SmsService.ServiceModel.Interfaces;

namespace SmsService
{
    public class Startup : ModularStartup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public new void ConfigureServices(IServiceCollection services)
        {
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseServiceStack(new AppHost
            {
                AppSettings = new NetCoreAppSettings(Configuration)
            });
        }
    }

    public class AppHost : AppHostBase
    {
        public AppHost() : base("SmsService", typeof(ServiceInterface.SmsService).Assembly) { }

        private Feature disabledFeatures = Feature.Csv | Feature.Jsv;
        // Configure your AppHost with the necessary configuration and dependencies your App needs
        public override void Configure(Container container)
        {
            SetConfig(new HostConfig
            {
                EnableFeatures = Feature.All.Remove(disabledFeatures),
                DefaultRedirectPath = "/metadata",
                DebugMode = AppSettings.Get(nameof(HostConfig.DebugMode), false),
            });
            JsConfig.Init(new Config
            {
                DateHandler = DateHandler.ISO8601
            });
            container.Register<ISmsRepository>(c => new SmsRepository());
            container.Register<ISmsSender>(c => new SmsSender(c.Resolve<ISmsRepository>()));
        }
    }
}
