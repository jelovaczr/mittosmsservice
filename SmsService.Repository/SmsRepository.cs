﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SmsService.Repository.Models;
using SmsService.ServiceModel;
using SmsService.ServiceModel.Interfaces;
using SmsService.ServiceModel.Types;

namespace SmsService.Repository
{
    public class SmsRepository : ISmsRepository
    {
        public async Task<List<CountryResponse>> GetCountries()
        {
            await using var context = new SmsDbContext();
            return await context.Countries.Select(c => new CountryResponse
            {
                MobileCountryCode = c.MobileCountryCode,
                CountryCode = c.CountryCode,
                PricePerSms = c.PricePerSms,
                Name = c.Name
            }).ToListAsync();
        }

        public async Task<CountryResponse> GetCountryByCountryCode(string countryCode)
        {
            await using var context = new SmsDbContext();
            var dbModel = await context.Countries.FirstOrDefaultAsync(c => c.CountryCode == countryCode);
            return dbModel == null ? null : new CountryResponse
            {
                MobileCountryCode = dbModel.MobileCountryCode,
                CountryCode = dbModel.CountryCode,
                Name = dbModel.Name,
                PricePerSms = dbModel.PricePerSms
            };
        }

        public async Task<SendStatus> SaveSms(SmsDTO sms)
        {
            try
            {
                await using var context = new SmsDbContext();
                await context.ShortMessages.AddAsync(new SmsDbModel
                {
                    To = sms.To,
                    From = sms.From,
                    Text = sms.Text,
                    CountryCode = context.Countries.Find(sms.CountryCode),
                    DateTime = DateTime.Now
                });
                await context.SaveChangesAsync();
                return SendStatus.Success;
            }
            catch (Exception e)
            {
                return SendStatus.Failed;
            }
        }

        public async Task<List<SmsDTO>> RetrieveMessages(DateTime dateFrom, DateTime dateTo, int skip = 0, int take = 0, List<string> countryCodes = null)
        {
            await using var context = new SmsDbContext();
            var query =  context.ShortMessages
                .Where(sm => sm.DateTime > dateFrom && sm.DateTime < dateTo)
                .Skip(skip);
            if(take > 0)
                query = query.Take(take);
            if (countryCodes != null && countryCodes.Any())
                query = query.Where(q => q.CountryCode != null && countryCodes.Contains(q.CountryCode.MobileCountryCode));
            return await query.Select(x => new SmsDTO
                {
                    DateTime = x.DateTime,
                    CountryCode = x.CountryCode == null ? null : x.CountryCode.CountryCode,
                    MobileCountryCode = x.CountryCode == null ? null : x.CountryCode.MobileCountryCode,
                    To = x.To,
                    From = x.From,
                    Text = x.Text,
                    Price = x.CountryCode == null ? 0 : x.CountryCode.PricePerSms,
                    State = SendStatus.Success
                }).ToListAsync();
        }
    }
}
