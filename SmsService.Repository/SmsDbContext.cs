﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using SmsService.Repository.Models;

namespace SmsService.Repository
{
    public class SmsDbContext : DbContext
    {
        public SmsDbContext(DbContextOptions<SmsDbContext> options) : base(options) {   }

        public SmsDbContext()
        {
            Database.EnsureCreated();
        }

        public DbSet<CountryDbModel> Countries { get; set; }
        public DbSet<SmsDbModel> ShortMessages { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            options.UseSqlite("Data Source=smsdatabase.db3");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<CountryDbModel>().HasData(
                new CountryDbModel { Name = "Germany", MobileCountryCode = "262", CountryCode = "49", PricePerSms = 0.055m },
                new CountryDbModel { Name = "Austria", MobileCountryCode = "232", CountryCode = "43", PricePerSms = 0.053m },
                new CountryDbModel { Name = "Poland", MobileCountryCode = "260", CountryCode = "48", PricePerSms = 0.032m }
            );
        }
    }

    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<SmsDbContext>
    {
        public SmsDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<SmsDbContext>();
            builder.UseSqlite("Data Source=smsdatabase.db3");
            return new SmsDbContext(builder.Options);
        }
    }
}
