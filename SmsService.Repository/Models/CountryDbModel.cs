﻿using System.ComponentModel.DataAnnotations;

namespace SmsService.Repository.Models
{
    public class CountryDbModel
    {
        public string MobileCountryCode { get; set; }
        [Key]
        public string CountryCode { get; set; }
        public string Name { get; set; }
        public decimal PricePerSms { get; set; }
    }
}
