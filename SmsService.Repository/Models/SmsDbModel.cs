﻿using System;
using SmsService.ServiceModel.Types;

namespace SmsService.Repository.Models
{
    public class SmsDbModel
    {
        public int Id { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string Text { get; set; }
        public CountryDbModel CountryCode { get; set; }
        public DateTime DateTime { get; set; }
        public decimal Price { get; set; }
    }
}
