using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PhoneNumbers;
using ServiceStack;
using SmsService.ServiceModel;
using SmsService.ServiceModel.Interfaces;
using SmsService.ServiceModel.Types;

namespace SmsService.ServiceInterface
{
    public class SmsService : Service
    {
        private readonly ISmsRepository _repo;
        private readonly ISmsSender _sender;

        public SmsService(ISmsRepository _repo, ISmsSender sender)
        {
            this._repo = _repo;
            _sender = sender;
        }

        public async Task<SendResponse> Get(Send request)
        {
            try
            {
                return await _sender.Send(new SmsDTO
                {
                    CountryCode = await GetCountryCode(request),
                    To = request.To,
                    From = request.From,
                    Text = request.Text
                });
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return new SendResponse {SendStatus = SendStatus.Failed };
            }
        }

        public async Task<List<CountryResponse>> Get(GetCountries request)
        {
            return await _repo.GetCountries();
        }

        public async Task<GetSentSmsResponse> Get(GetSentSms request)
        {
            var result = await _repo.RetrieveMessages(request.DateTimeFrom, request.DateTimeTo, request.Skip, request.Take);
            return new GetSentSmsResponse
            {
                Items = result.Select(m => new Sms
                {
                    DateTime = m.DateTime,
                    MobileCountryCode = m.MobileCountryCode,
                    To = m.To,
                    From = m.From,
                    Price = m.Price,
                    State = m.State
                }).ToArray(),
                TotalCount = result.Count
            };
        }

        public async Task<GetStatisticsResponse> Get(GetStatistics request)
        {
            var result = await _repo.RetrieveMessages(request.DateFrom, request.DateTo, countryCodes: request.MobileCountryCodes != null ? request.MobileCountryCodes.ToList() : null);
            return new GetStatisticsResponse
            {
                StatisticRecords = result.Select(m => new StatisticRecord
                {
                    MobileCountryCode = m.MobileCountryCode,
                    Day = m.DateTime.Date,
                    PricePerSms = m.Price
                }).ToArray(),
                Count = result.Count,
                TotalPrice = result.Sum(m => m.Price)
            };
        }

        private async Task<string> GetCountryCode(Send request)
        {
            var phoneUtil = PhoneNumberUtil.GetInstance();
            try
            {
                var numberProto = phoneUtil.Parse(request.To, "");
                var countryCode = numberProto.CountryCode.ToString();
                var country = await _repo.GetCountryByCountryCode(countryCode);
                return country?.CountryCode;
            }
            catch (NumberParseException e)
            {
                throw new InvalidOperationException("Phone number could not be parsed");
            }
        }
    }
}
