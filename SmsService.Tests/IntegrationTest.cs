using Funq;
using ServiceStack;
using NUnit.Framework;
using SmsService.ServiceInterface;
using SmsService.ServiceModel;
using SmsService.ServiceModel.Types;

namespace SmsService.Tests
{
    public class IntegrationTest
    {
        const string BaseUri = "http://localhost:2000/";
        private readonly ServiceStackHost appHost;

        class AppHost : AppSelfHostBase
        {
            public AppHost() : base(nameof(IntegrationTest), typeof(ServiceInterface.SmsService).Assembly) { }

            public override void Configure(Container container)
            {
            }
        }

        public IntegrationTest()
        {
            appHost = new AppHost()
                .Init()
                .Start(BaseUri);
        }

        [OneTimeTearDown]
        public void OneTimeTearDown() => appHost.Dispose();

        public IServiceClient CreateClient() => new JsonServiceClient(BaseUri);

        [Test]
        public void Can_call_Hello_Service()
        {
            var client = CreateClient();

            var response = client.Get(new Send());

            Assert.That(response.SendStatus, Is.EqualTo(SendStatus.Success));
        }
    }
}