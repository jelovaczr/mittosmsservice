using NUnit.Framework;
using ServiceStack;
using ServiceStack.Testing;
using SmsService.ServiceModel;
using SmsService.ServiceModel.Types;

namespace SmsService.Tests
{
    public class UnitTest
    {
        private readonly ServiceStackHost appHost;

        public UnitTest()
        {
            appHost = new BasicAppHost().Init();
            appHost.Container.AddTransient<ServiceInterface.SmsService>();
        }

        [OneTimeTearDown]
        public void OneTimeTearDown() => appHost.Dispose();

        [Test]
        public void Can_call_MyServices()
        {
            var service = appHost.Container.Resolve<ServiceInterface.SmsService>();

            var response = service.Get(new Send());

            //Assert.AreEqual(response.SendStatus, SendStatus.Success);
        }
    }
}
