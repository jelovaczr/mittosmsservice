﻿using System;

namespace SmsService.ServiceModel.Types
{
    public class SmsDTO
    {
        public string From { get; set; }
        public string To { get; set; }
        public string Text { get; set; }
        public string CountryCode { get; set; }
        public string MobileCountryCode { get; set; }
        public DateTime DateTime { get; set; }
        public decimal Price { get; set; }
        public SendStatus State { get; set; }
    }
}
