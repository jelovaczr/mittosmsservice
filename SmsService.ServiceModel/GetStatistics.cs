﻿using System;
using System.Runtime.Serialization;
using ServiceStack;

namespace SmsService.ServiceModel
{
    [Route("/statistics")]
    public class GetStatistics : IReturn<GetStatisticsResponse>
    {
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        [DataMember(Name = "mccList")]
        public string[] MobileCountryCodes { get; set; }
    }

    public class GetStatisticsResponse
    {
        public StatisticRecord[] StatisticRecords { get; set; }
        public int Count { get; set; }
        public decimal TotalPrice { get; set; }
    }

    public class StatisticRecord
    {
        public DateTime Day { get; set; }
        [DataMember(Name = "mcc")]
        public string MobileCountryCode { get; set; }
        [DataMember(Name = "pricePerSMS")]
        public decimal PricePerSms { get; set; }

    }
}
