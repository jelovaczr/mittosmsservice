﻿using System;
using System.Runtime.Serialization;
using ServiceStack;
using SmsService.ServiceModel.Types;

namespace SmsService.ServiceModel
{
    [Route("/sms/sent")]
    public class GetSentSms : IReturn<GetSentSmsResponse>
    {
        public DateTime DateTimeFrom { get; set; }
        public DateTime DateTimeTo { get; set; }
        public int Skip { get; set; }
        public int Take { get; set; }
    }

    public class GetSentSmsResponse
    {
        public int TotalCount { get; set; }
        public Sms[] Items { get; set; }
    }
    
    public class Sms
    {
        public DateTime DateTime { get; set; }
        [DataMember(Name = "mcc")]
        public string MobileCountryCode { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public decimal Price { get; set; }
        public SendStatus State { get; set; }

    }
}
