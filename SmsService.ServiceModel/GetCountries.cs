﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using ServiceStack;

namespace SmsService.ServiceModel
{
    [Route("/countries")]
    public class GetCountries : IReturn<CountryResponse> { }

    public class CountryResponse
    {
        [DataMember(Name = "mcc")]
        public string MobileCountryCode { get; set; }
        [DataMember(Name = "cc")]
        public string CountryCode { get; set; }
        public string Name { get; set; }
        [DataMember(Name = "pricePerSMS")]
        public decimal PricePerSms { get; set; }
    }
}
