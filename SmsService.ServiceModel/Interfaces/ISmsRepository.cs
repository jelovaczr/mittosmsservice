﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SmsService.ServiceModel.Types;

namespace SmsService.ServiceModel.Interfaces
{
    public interface ISmsRepository
    {
        Task<List<CountryResponse>> GetCountries();
        Task<CountryResponse> GetCountryByCountryCode(string countryCode);
        Task<SendStatus> SaveSms(SmsDTO sms);
        Task<List<SmsDTO>> RetrieveMessages(DateTime dateFrom, DateTime dateTo, int skip = 0, int take = 0,
            List<string> countryCodes = null);
    }
}
