﻿using System.Threading.Tasks;
using SmsService.ServiceModel.Types;

namespace SmsService.ServiceModel.Interfaces
{
    public interface ISmsSender
    {
        Task<SendResponse> Send(SmsDTO sms);
    }
}
