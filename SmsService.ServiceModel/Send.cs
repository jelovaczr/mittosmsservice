using ServiceStack;
using SmsService.ServiceModel.Types;

namespace SmsService.ServiceModel
{

    [Route("/Sms/Send")]
    public class Send : IReturn<SendResponse>
    {
        public string From { get; set; }
        public string To { get; set; }
        public string Text { get; set; }
    }

    public class SendResponse
    {
        public SendStatus SendStatus { get; set; }
    }
}
