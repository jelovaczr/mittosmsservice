# web

Mitto SMS Service

[![](https://gitlab.com/jelovaczr/mittosmsservice/-/raw/1223988723bf948d27089fa7a4749f55bb8a6580/Screenshot.png)]

> Installation steps:

	$ cd to the solution folder

    $ docker build -t smsservice:v1 .

    $ docker run -it --rm --publish 8080:80 --detach --name smsservice smsservice:v1 .
	
	Access web api on localhost:8080

